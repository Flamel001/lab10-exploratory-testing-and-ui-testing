context('Ozon', () => {
  

  describe('Checking interface unauthorised', () => {

    beforeEach(() => {
      cy.visit('https://www.ozon.ru/')
    })


    it('Check orders', () => {
      cy.get('[data-widget="orderInfo"]').click()
      cy.contains('Вы не авторизованы')
      cy.contains('Для доступа к личному кабинету необходимо')
      cy.get('[data-widget="loginButton"]').click()
      cy.get('iframe[id="authFrame"]')
      
    })

    it('Check catalog', () => {
      //click catalog button to see catalog
      cy.get('[data-widget="catalogMenu"]').click()

      //check Electronics catalog
      cy.contains('Электроника').trigger('mouseenter')
      cy.contains('Телефоны и смарт-часы')
      cy.contains('Наушники и аудиотехника')
      cy.contains('Фото- и видеокамеры')
      cy.contains('Навигаторы')

      //check clothers catalog
      cy.contains('Одежда, обувь и аксессуары').trigger('mouseenter')
      cy.contains('Мужчинам')
      cy.contains('Женщинам')
      cy.contains('Детям')
      cy.contains('Путешествия');

      // click catalogButton again and check catalog exit
      cy.get('[data-widget="catalogMenu"]').click()
      cy.contains('Телефоны и смарт-часы').should('not.exist')

    })

    it('Check login popup', () => {
      cy.get('[data-widget="profileMenuAnonymous"]').trigger('mouseover')
      cy.contains('Войдите, чтобы делать покупки, отслеживать заказы и пользоваться персональными скидками и баллами')
      cy.contains('Войти или зарегистрироваться').click()
      cy.get('iframe[id="authFrame"]')
    })

  })
})